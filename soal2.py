from datetime import datetime
from airflow import DAG
from airflow.operators.postgres_operator import PostgresOperator
import airflow

from datetime import timedelta

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'kitabisa_soal2',
    default_args=default_args,
    description='soal nomor 2 kitabisa',
    schedule_interval=timedelta(days=1),
)

# t0 = PostgresOperator(
#     task_id ='create_database',
#     sql='CREATE DATABASE kitabisa;',
#     dag=dag,
# )

t1 = PostgresOperator(
        task_id='create_table',
        sql='soal1.sql',
        dag=dag,
    )
t1