CREATE TABLE  product (
	ID serial NOT NULL PRIMARY KEY,
	data jsonb NOT NULL
);

INSERT INTO product (data)
VALUES
	( '[{"id":"0001","type":"donut","name":"Cake","ppu":0.55,"batters":{},"topping":[]},{},{"id":"0003","type":"donut","name":"Old Fashioned","ppu":0.55,"batters":{"batter":[{"id":"1001","type":"Regular"},{"id":"1002","type":"Chocolate"}]},"topping":[]}]'
	);

SELECT * FROM 

(SELECT d.value->>'id' AS id, d.value->>'type'AS type ,d.value->>'name' AS name,d.value->>'ppu' AS ppu
	FROM product
	JOIN LATERAL jsonb_array_elements(data) d
	ON true) AS g 
LEFT JOIN
	(SELECT tp.value->>'id' AS topping_id, tp.value->>'type' AS toppings
	from product
	join lateral jsonb_array_elements(data->0->'topping') tp
	on true) tpg ON (g.id = g.id)
LEFT JOIN
(SELECT btr.value->>'type' AS tipe,btr.value->>'id' as btr_id
FROM product
JOIN LATERAL jsonb_array_elements(data->0->'batters'->'batter') btr
on true) dd
	ON (tpg.topping_id = tpg.topping_id)