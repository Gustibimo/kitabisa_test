def get_primes(n):
    primes = []
    n = 2
    while len(primes) != n:
        for i in range(2, n // 2 + 1):
            if n % i == 0:
                break
        else:
            primes.append(n)
        n += 1

    return primes

n = 4; 
print(get_primes(n))